# Self-Driving Car JS Tutorial

A neural network for self-driving car simulation using JavaScript with no libraries. 

Just an attempt to follow this tutorial with the idea of trying some modifications later: 

https://www.youtube.com/watch?v=Rs_rAxEsAvI (original code)


Goal: to learn how to implement the car driving mechanics, how to define the environment, how to simulate some sensors, how to detect collisions, and how to make the car control itself using a neural network.
